<?php
namespace ExtDirect;

use Psr\Http\Message\ResponseInterface;

/**
 * Class Discoverer
 * @package ExtDirect
 */
class Discoverer
{
    /** @var  ClassMapper */
    protected $mapper;

    /** @var string */
    protected $apiUrl;

    /** @var string */
    protected $apiId;

    /** @var string */
    protected $apiName;

    /** @var string */
    protected $apiNamespace;

    /** @var int */
    protected $apiTimeout;


    public function __construct(ClassMapper $classMapper, array $config)
    {
        $this->mapper = $classMapper;

        $this->apiUrl = $config['api']['url'];
        $this->apiId = $config['api']['id'];
        $this->apiName = $config['api']['name'];
        $this->apiNamespace = $config['api']['namespace'];
        $this->apiTimeout = $config['api']['timeout'];
    }

    /**
     * Build API declaration
     *
     * @param $classMap
     * @return array
     */
    protected function buildApi($classMap)
    {
        $api = [
            'url' => $this->apiUrl,
            'id' => $this->apiId,
            'type' => 'remoting',
            'namespace' => $this->apiNamespace,
            'timeout' => $this->apiTimeout,
            'actions' => []
        ];

        foreach($classMap as $actionName => $actionProps) {
            array_walk($actionProps['methods'], function(&$method) {
                if (isset($method['resultTransformer'])) {
                    unset($method['resultTransformer']);
                }
            });

            $api['actions'][$actionName] = array_values($actionProps['methods']);
        }

        return $api;
    }

    /**
     * Execute discovery process
     *
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws \ReflectionException
     */
    public function discover(ResponseInterface $response) : ResponseInterface
    {
        $classMap = $this->mapper->map();
        $api      = $this->buildApi($classMap);

        $body = sprintf('Ext.ns("%1$s"); %1$s.%2$s=%3$s;',
            $this->apiNamespace,
            $this->apiName,
            json_encode($api, \JSON_UNESCAPED_UNICODE));

        $response->getBody()->write($body);
        return $response->withHeader('Content-Type', 'text/javascript');
    }
}