<?php

namespace ExtDirect;

class ActionResult
{
    /** @var mixed */
    public $result;

    /**
     * ActionResult constructor.
     * @param mixed $result
     */
    public function __construct($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }
}