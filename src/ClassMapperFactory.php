<?php

namespace ExtDirect;

use Psr\Container\ContainerInterface;
use \RuntimeException;

class ClassMapperFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        if (!isset($config['extdirect'])) {
            throw new RuntimeException('Config key extdirect not found');
        }

        $paths = $config['extdirect']['paths'];
        $cacheId = $config['extdirect']['api']['id'];
        $cacheLifetime = $config['extdirect']['cacheLifetime'];

        return new ClassMapper($paths, null, $cacheId, $cacheLifetime);
    }
}