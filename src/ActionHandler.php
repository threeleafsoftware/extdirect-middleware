<?php
namespace ExtDirect;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use \Throwable;
use \OutOfBoundsException;
use \RuntimeException;

class ActionHandler
{
    /** @var ServerRequestInterface */
    protected $request;

    /** @var string */
    protected $name;

    /** @var string */
    protected $class;

    /** @var string */
    protected $classFile;

    /** @var string */
    protected $method;

    /** @var array */
    protected $arguments;

    /** @var int */
    protected $tid;

    /** @var bool */
    protected $formHandler;

    /** @var array */
    protected $files = [];

    /** @var callable|null  */
    protected $resultTransformer;

    /**
     * Action constructor.
     *
     * @param ServerRequestInterface $request
     * @param string $name
     * @param string $class
     * @param string $classFile
     * @param string $method
     * @param array $arguments
     * @param int $tid
     * @param callable|null $resultTransformer
     * @param bool $formHandler
     * @param array $files
     */
    public function __construct(ServerRequestInterface $request, string $name, string $class, string $classFile,
                                string $method, ?array $arguments, int $tid, ?callable $resultTransformer,
                                bool $formHandler = false, array $files = [])
    {
        $this->request = $request;
        $this->name = $name;
        $this->class = $class;
        $this->classFile = $classFile;
        $this->method = $method;
        $this->arguments = $arguments ?: [];
        $this->tid = $tid;
        $this->files = $files;
        $this->formHandler = $formHandler;
        $this->resultTransformer = $resultTransformer;
    }

    /**
     * @param ContainerInterface $container
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function run(ContainerInterface $container) : array
    {
        $response = array(
            'action'  => $this->name,
            'method'  => $this->method,
            'result'  => null,
            'type'    => 'rpc',
            'tid'     => $this->tid
        );

        try {
            $result = $this->callAction($container);
            if (! is_null($result)) {
                $response['result'] = $result->getResult();
            }
        }
        catch (ActionException $e) {
            $response['type'] = 'exception';
            $response['message'] = $e->getExceptionData();
            $response['where'] = $e->getTraceAsString();
        }
        catch (Throwable $e) {
            $response['type'] = 'exception';
            $response['message'] = $e->getMessage();
            $response['where'] = $e->getTraceAsString();
        }

        return $response;
    }

    /**
     * @return bool
     */
    public function isFormHandler() : bool
    {
        return $this->formHandler;
    }

    /**
     * @return bool
     */
    public function isUpload() : bool
    {
        return count($this->files) > 0;
    }

    /**
     * @param $key
     * @return bool
     */
    protected function hasFile($key) : bool
    {
        return isset($this->files[$key]);
    }

    /**
     * @param $key
     * @return UploadedFileInterface
     * @throws OutOfBoundsException
     */
    protected function getFile($key) : UploadedFileInterface
    {
        if ($this->$this->hasFile($key)) {
            return $this->files[$key];
        }

        throw new OutOfBoundsException(sprintf('File upload key `%s` is invalid', $key));
    }

    /**
     * @param ContainerInterface $container
     * @return ActionResult|null
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \ReflectionException
     */
    protected function callAction(ContainerInterface $container) : ?ActionResult
    {
        ClassMapper::includeFile($this->classFile);

        $reflectedMethod = new \ReflectionMethod($this->class, $this->method);

        $arguments = [];
        if ($this->isFormHandler()) {
            foreach ($reflectedMethod->getParameters() as $reflectedParam) {
                $paramName  = $reflectedParam->getName();
                $paramValue = null;

                if (isset($this->arguments[$paramName])) {
                    $paramValue = $this->arguments[$paramName];
                } elseif ($this->isUpload() && $this->hasFile($paramName)) {
                    $paramValue = $this->getFile($paramName);
                } elseif ($reflectedParam->isDefaultValueAvailable()) {
                    $paramValue = $reflectedParam->getDefaultValue();
                }

                $arguments[] = $paramValue;
            }
        } else {
            $arguments = $this->arguments;
        }

        $result = null;

        if (! $container->has($this->class)) {
            throw new RuntimeException(sprintf('Container does not have an %s entry', $this->class));
        }

        /** @var ActionInterface $instance */
        $instance = $container->get($this->class);

        if (true === $instance->beforeAction($this->request, ...$arguments)) {
            if (!empty($arguments)) {
                $result = call_user_func_array(array($instance, $this->method), $arguments);
            } else {
                $result = call_user_func(array($instance, $this->method));
            }

            if (is_callable($this->resultTransformer)) {
                $result = call_user_func($this->resultTransformer, $this, $result);
            }

            return $result;
        }

        throw new ActionException('Action aborted', -32001, 'ActionAborted');
    }
}