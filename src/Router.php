<?php
namespace ExtDirect;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\Response;

/**
 * Class Discoverer
 * @package ExtDirect
 */
class Router
{
    /** @var  ClassMapper */
    protected $mapper;

    /** @var string */
    protected $apiId;

    public function __construct(ClassMapper $classMapper, array $config)
    {
        $this->mapper = $classMapper;
        $this->apiId = $config['api']['id'];
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool
     */
    public function isFormRequest(ServerRequestInterface $request)
    {
        $contentTypes = $request->getHeader('Content-Type');
        foreach($contentTypes as $contentType) {
            if (false !== strpos($contentType, 'application/x-www-form-urlencoded')) {
                return true;
            }
            if (false !== strpos($contentType, 'multipart/form-data')) {
                return true;
            }
        }
        return false;
    }

    public function isUpload(ServerRequestInterface $request)
    {
        $contentTypes = $request->getHeader('Content-Type');
        foreach($contentTypes as $contentType) {
            if (false !== strpos($contentType, 'multipart/form-data')) {
                return (count($request->getUploadedFiles()) > 0);
            }
        }
        return false;
    }

    /**
     * @param $actionMap
     * @param $method
     * @return bool
     */
    protected function methodIsAllowed(array $actionMap, $method)
    {
        return isset($actionMap['methods'][$method]);
    }

    /**
     *
     * @param ServerRequestInterface $request
     * @param array $classMap
     * @return ActionHandler[]
     */
    protected function getActions(ServerRequestInterface $request, array $classMap)
    {
        $actions = [];

        if ($this->isFormRequest($request)) {
            $call = $request->getParsedBody();

            $actionName = $call['extAction'];
            if (!isset($classMap[$actionName])) {
                throw new \InvalidArgumentException(sprintf('Unknow action %s', $actionName));
            }
            $actionMap = $classMap[$actionName];

            if (!$this->methodIsAllowed($actionMap, $call['extMethod'])) {
                throw new \InvalidArgumentException(sprintf('Method %s is not allowed', $call['extMethod']));
            }

            $postVars = $call;
            foreach (['extAction', 'extMethod', 'extTID', 'extUpload', 'extType'] as $extVar) {
                if (isset($postVars[$extVar])) {
                    unset($postVars[$extVar]);
                }
            }

            $resultTransformer = null;
            if (is_callable($actionMap['methods'][$call['extMethod']])) {
                $resultTransformer = $actionMap['methods'][$call['extMethod']];
            }

            $actions[] = new ActionHandler($request, $actionMap['action'], $actionMap['class'], $actionMap['file'],
                $call['extMethod'], $postVars, $call['extTID'], $resultTransformer,true,
                $request->getUploadedFiles());

            return $actions;
        }
        $calls = json_decode($request->getBody()->getContents());
        $calls = is_array($calls) ? $calls : [$calls];

        foreach($calls as $call) {
            if (isset($call->type) && $call->type == 'rpc') {
                $actionName = $call->action;
                if (!isset($classMap[$actionName])) {
                    throw new \InvalidArgumentException(sprintf('Unknow action %s', $actionName));
                }
                $actionMap = $classMap[$actionName];

                if (!$this->methodIsAllowed($actionMap, $call->method)) {
                    throw new \InvalidArgumentException(sprintf('Method %s is not allowed', $call->method));
                }

                $resultTransformer = null;
                if (is_callable($actionMap['methods'][$call->method])) {
                    $resultTransformer = $actionMap['methods'][$call->method];
                }

                $actions[] = new ActionHandler($request, $actionMap['action'], $actionMap['class'], $actionMap['file'],
                    $call->method, $call->data, $call->tid, $resultTransformer);
            }
        }

        return $actions;
    }

    /**
     *
     * @param ContainerInterface $container
     * @param ServerRequestInterface|null $request
     * @param ResponseInterface|null $response
     * @return ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \ReflectionException
     */
    public function route(ContainerInterface $container,
                          ServerRequestInterface $request,
                          ResponseInterface $response) : ResponseInterface
    {
        $request  = $request ?: ServerRequestFactory::fromGlobals();
        $response = $response ?: new Response();
        $classMap = $this->mapper->map();

        $actionsResults = [];
        $actions = $this->getActions($request, $classMap);
        $upload = false;
        foreach ($actions as $action) {
            $actionsResults[] = $action->run($container);
            if ($action->isUpload()) $upload = true;
        }

        if ($upload) {
            $result = sprintf('<html><body><textarea>%s</textarea></body></html>',
                preg_replace('/&quot;/', '\\&quot;', json_encode($actionsResults[0], \JSON_UNESCAPED_UNICODE)));

            $response->getBody()->write($result);
            $response = $response->withHeader('Content-Type', 'text/html');
        } else {
            if (count($actionsResults) == 1) {
                $response->getBody()->write(json_encode($actionsResults[0], \JSON_UNESCAPED_UNICODE));
            } else {
                $response->getBody()->write(json_encode($actionsResults, \JSON_UNESCAPED_UNICODE));
            }
            $response = $response->withHeader('Content-Type', 'application/json');
        }

        return $response;
    }
}