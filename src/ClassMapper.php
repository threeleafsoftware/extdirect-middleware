<?php

namespace ExtDirect;

use Nette\Reflection\AnnotationsParser;
use Doctrine\Common\Cache\CacheProvider;
use \RecursiveDirectoryIterator;
use \RecursiveIteratorIterator;
use \RegexIterator;
use \RecursiveRegexIterator;

class ClassMapper
{
    /** @var array */
    protected $paths;

    /** @var CacheProvider */
    protected $cacheProvider;

    /** @var null|string */
    protected $cacheId;

    /** @var int */
    protected $cacheLifetime;

    /**
     * ClassMapper constructor.
     *
     * @param array $paths
     * @param CacheProvider|null $cacheProvider
     * @param null|string $cacheId
     * @param int $cacheLifetime
     */
    public function __construct(array $paths, ?CacheProvider $cacheProvider, ?string $cacheId, int $cacheLifetime = 0)
    {
        if (empty($paths)) {
            throw new \InvalidArgumentException('The paths array cannot be empty');
        }

        foreach ($paths as $path) {
            if (!is_dir($path)) {
                throw new \InvalidArgumentException(sprintf('%s is not a path to a directory', $path));
            }

            if (!is_readable($path)) {
                throw new \InvalidArgumentException(sprintf('%s is not a path to a readable directory', $path));
            }
        }

        if (!is_null($cacheProvider) && is_null($cacheId)) {
            throw new \InvalidArgumentException('The cacheProvider parameter also requires the cacheId parameter');
        }

        $this->paths = $paths;
        $this->cacheProvider = $cacheProvider;
        $this->cacheId = $cacheId;
        $this->cacheLifetime = $cacheLifetime;
    }

    /**
     * @param string $file
     */
    public static function includeFile(string $file) : void {
        /** @noinspection PhpIncludeInspection */
        include_once $file;
    }

     /**
     * @param $path
     * @return array
     */
    public static function loadDir($path)
    {
        $Directory = new RecursiveDirectoryIterator($path);
        $Iterator = new RecursiveIteratorIterator($Directory);
        $RegexIterator = new RegexIterator($Iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);

        $files = [];
        foreach ($RegexIterator as $item) {
            $files[] = $item[0];
        }

        return $files;
    }

    /**
     * @param \ReflectionClass $reflectedClass
     * @return array
     */
    public static function getMethods(\ReflectionClass $reflectedClass)
    {
        $methods = [];

        foreach($reflectedClass->getMethods() as $reflectedMethod) {
            if (! $reflectedMethod->isPublic()
                || $reflectedMethod->isConstructor()
                || $reflectedMethod->isDestructor()
                || $reflectedMethod->isAbstract()
            ) {
                continue;
            }

            $methodAnnotations = AnnotationsParser::getAll($reflectedMethod);
            if (isset($methodAnnotations['ExtDirect\Ignore'])) {
                continue;
            }
            $method = [
                'name'   => $reflectedMethod->getName(),
                'len' => $reflectedMethod->getNumberOfParameters()
            ];

            if (isset($methodAnnotations['ExtDirect\FormHandler'])) {
                $method['formHandler'] = true;
                unset($method['len']);
            }

            if (isset($methodAnnotations['ExtDirect\ResultTransformer'])) {
                if (is_array($methodAnnotations['ExtDirect\ResultTransformer']) &&
                    is_string($methodAnnotations['ExtDirect\ResultTransformer'][0])) {
                    $method['resultTransformer'] = $methodAnnotations['ExtDirect\ResultTransformer'][0];
                }
            }

            $methods[$method['name']] = $method;
        }

        return $methods;
    }

    /**
     * Scan paths and get actions
     *
     * @return array
     * @throws \ReflectionException
     */
    public function map()
    {
        $cache         = $this->cacheProvider;
        $cacheId       = $this->cacheId;
        $cacheLifetime = $this->cacheLifetime;

        if ($cache && $cache->contains($cacheId)) {
            return $cache->fetch($cacheId);
        }

        $files = $classMap = [];
        foreach ($this->paths as $path) {
            $files = array_merge($files, static::loadDir($path));
        }

        foreach ($files as $file) {
            $fileContent = file_get_contents($file);
            /** @noinspection PhpInternalEntityUsedInspection */
            $classes = array_keys(AnnotationsParser::parsePhp($fileContent));

            static::includeFile($file);

            foreach ($classes as $className) {
                $class = new \ReflectionClass($className);
                if (!$class->isInstantiable()) {
                    continue;
                }

                $classAnnotations = AnnotationsParser::getAll($class);
                if (!in_array(ActionInterface::class, class_implements($className))) {
                    continue;
                }

                $methods = static::getMethods($class);

                $classAlias = null;
                if (isset($classAnnotations['ExtDirect\Alias'])) {
                    if (is_array($classAnnotations['ExtDirect\Alias']) &&
                        is_string($classAnnotations['ExtDirect\Alias'][0])) {
                        $classAlias = $classAnnotations['ExtDirect\Alias'][0];
                    }
                }

                $actionName = $classAlias ?: str_replace('\\', '.', $className);

                $classMap[$actionName]['action'] = $actionName;
                $classMap[$actionName]['class'] = $className;
                $classMap[$actionName]['file'] = $file;
                $classMap[$actionName]['methods'] = $methods;
            }
        }

        if ($cache) {
            $cache->save($cacheId, $classMap, $cacheLifetime);
        }

        return $classMap;
    }
}