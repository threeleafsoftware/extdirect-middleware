<?php

namespace ExtDirect;

class ActionException extends \Exception
{
    public $exceptionData = [];

    public function __construct(string $errorMessage, int $errorCode, string $errorName, array $data = [])
    {
        parent::__construct($errorMessage, $errorCode);

        $this->exceptionData = array_merge($data, [
            'message' => $errorMessage,
            'name' => $errorName,
            'code' => $errorCode,
        ]);
    }

    /**
     * @return array
     */
    public function getExceptionData()
    {
        return $this->exceptionData;
    }
}