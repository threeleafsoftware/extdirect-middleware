<?php
namespace ExtDirect;

use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response;
use Psr\Container\ContainerInterface;

class DiscovererMiddleware implements MiddlewareInterface
{
    /** @var Discoverer */
    protected $discoverer;

    /** @var ContainerInterface */
    private $container;

    /**
     * DiscovererMiddleware constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        $this->discoverer = $container->get(Discoverer::class);
        $this->container = $container;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * to the next middleware component to create the response.
     *
     * @param Request $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \ReflectionException
     */
    public function process(Request $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $response = new Response();
        return $this->discoverer->discover($response);
    }
}