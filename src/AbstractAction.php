<?php

namespace ExtDirect;

use Psr\Http\Message\ServerRequestInterface;

abstract class AbstractAction implements ActionInterface
{
    /** @var ServerRequestInterface */
    protected $request;

    /**
     * @param ServerRequestInterface $request
     * @ExtDirect\Ignore
     */
    public function setRequest(ServerRequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * @param ServerRequestInterface $request
     * @param array ...$params
     * @return bool
     * @ExtDirect\Ignore
     */
    public function beforeAction(ServerRequestInterface $request, ...$params) : bool
    {
        return true;
    }
}