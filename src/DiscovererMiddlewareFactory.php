<?php

namespace ExtDirect;

use Psr\Container\ContainerInterface;

class DiscovererMiddlewareFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new DiscovererMiddleware($container);
    }
}