<?php
namespace ExtDirect;

use Psr\Http\Message\ServerRequestInterface;

interface ActionInterface
{
    /**
     * @param ServerRequestInterface $request
     */
    public function setRequest(ServerRequestInterface $request);

    public function beforeAction(ServerRequestInterface $request, ...$params) : bool;
}