<?php

namespace ExtDirect;

use Psr\Container\ContainerInterface;
use \RuntimeException;

class DiscovererFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        if (!isset($config['extdirect'])) {
            throw new RuntimeException('Config key extdirect not found');
        }
        $mapper = $container->get(ClassMapper::class);

        return new Discoverer($mapper, $config['extdirect']);
    }
}