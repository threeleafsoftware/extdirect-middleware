<?php

namespace ExtDirect;

use Psr\Container\ContainerInterface;

class RouterMiddlewareFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new RouterMiddleware($container);
    }
}