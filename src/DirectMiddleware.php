<?php
/**
 * Created by PhpStorm.
 * User: danielbraga
 * Date: 31/07/2017
 * Time: 16:39
 */

namespace ExtDirect;

use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;

class DirectMiddleware implements MiddlewareInterface
{
    /** @var Discoverer */
    protected $discoverer;

    public function __construct(Discoverer $discoverer)
    {
        $this->discoverer = $discoverer;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * to the next middleware component to create the response.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return void
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {
        if ($request->getMethod() == 'GET') {
            return;
        }
    }
}